package main

import (
	"fmt"
	"math"
	"math/rand"
	//	"image"
	//	"image/draw"
	//	"io"
	"image"
	"image/color"
	_ "image/png"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/jinzhu/copier"
	"golang.org/x/image/colornames"
	//	"io"
	//	"log/err"
)

//ChooseChar is used for the character select screen
func ChooseChar(charac int) (float64, float64) {
	switch charac {
	case 0:
		return 165.0, 165.0
	case 1:
		return 950.0, 225.0
	case 2:
		return 408.0, 300.0
	case 3:
		return 658.0, 232.0
	}
	return 0, 0
}

//LoadPicture Loads the picture from file.
func LoadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Error opening file")
		panic(err)
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		fmt.Println("Error decoding file")
		panic(err)
	}
	return pixel.PictureDataFromImage(img), nil

}

//SliceFrames Slices a horizontal spritesheet.
func SliceFrames(sheet pixel.Picture, frameWidth float64) []pixel.Rect {
	var frames []pixel.Rect
	//this does what the next part grabs
	for x := 0.0; x+frameWidth <= sheet.Bounds().Max.X; x += frameWidth {
		frames = append(frames, pixel.R(
			x,
			0,
			x+frameWidth,
			sheet.Bounds().H(),
		))
	}
	return frames
}

//GetMonsters gives you the total number of monsters on any given level
func GetMonsters(pSkillPoints int, pNum int, pLvl int) int {
	monsters := pSkillPoints * pNum * pLvl
	return monsters
}

//define a level:
// monsters = players skill points * number of players * player level

// skill gain level = 2**2 XP per skill level

//GetGain gives you the amount of SP until your next skill level
func GetGain(pSkillLvl float64) float64 {
	SP := math.Pow(2, pSkillLvl)
	return SP
}

// player level XP = 6**2 per player level

//GetLvl gives you your XP to next level
func GetLvl(pLvl float64) float64 {
	XP := math.Pow(6, pLvl)
	return XP
}

//TODO
func GetAnim(chara int) []*pixel.Sprite {
	switch chara {

	}
	return nil
}

//BlobMonsters is a container for all monsters in a level
type BlobMonsters struct {
	Hp    int
	Mp    int
	Xpos  float64
	Ypos  float64
	Anims []*pixel.Sprite
}

//Player defines a character's base statistics and animations
type Player struct {
	Hp           float64
	Mp           float64
	Xpos         float64
	Ypos         float64
	EAttack      string
	QAttack      string
	SAttack      string
	ShAttack     string
	Idle         bool
	IdleAnims    []*pixel.Sprite
	IdleAnimName string
	EAnims       []*pixel.Sprite
	QAnims       []*pixel.Sprite
	SAnims       []*pixel.Sprite
	ShAnims      []*pixel.Sprite
	SpriteIndex  int
}

//ImportSprite imports and processes a sprite into a series of animation slices
func ImportSprite(path string, frameWidth float64) []*pixel.Sprite {
	Pic, err := LoadPicture(path)
	if err != nil {
		fmt.Println("Error loading the picture!")
	}
	var Frame []pixel.Rect
	Frame = SliceFrames(Pic, frameWidth)
	var Anims []*pixel.Sprite
	for i := 0; i < len(Frame); i++ {
		spriter := pixel.NewSprite(Pic, Frame[i])
		Anims = append(Anims, spriter)
	}
	return Anims
}

// Skills are CL=click(left), CR=click(right), MooshE, MooshQ=(E, Q) Shmoosh=(shift)
// Attacks can be combinations of all of the above
// Use skill points to unlock combo strikes. (ala saGa frontier) MPlayer
// Attacks use Mutators to give extra spice to that new arm slam attack
// IE, FireStrike, IceStrike, ShadowStrike, LightStrike
// Game Modes:
// OneTwoBoosht: choose three characters, and when you
// unlock the third, they BOOSHT onto the screen
// WaffleIron: choose two characters, and when you 'unlock'
// the second, they Boosht.
// TruckNutz And Coffee: One character, the whole story.
// Endless, same as OneTwoBoosht, but neverending enemies,
// and you scroll through all available characters.
// Skill Modes:
// Tactical Stat progression is a laid out path for each character.
// Frenetic Stat progression is random.
//

//Hitbox notes, when an attack is made, a hitbox will be generated
//that box will have a calculated strength and size, similar to a
//kamehameha(which can be duplicated by an ult) When a player has
//spent a certain amount of "power" they will automatically unlock
//the next character in their route. Jumping will have a purpose!
//When you're ready to boosht, jump and it will expend your "power"
//and boosht depending. *not sure on this, you should probably boosht
//a little on first switch, then a LOT on third.* When you die or run out of
//hp, you de-boosht with a portion of the health from the previous form
//depending on the character.

//also, as a note, booshting will not expend ultimagicks, so if you want
//to build it up with little punches and carry forward until you're full
//form? DO EEET!

//UI Notes:
// When a power is used/character is changed, the UI for the power
// should react. From Woobie to Bloobie would have crystal type to
// water/blue type attacks
// When they're used, have a standard grey colour with a 'charging' colour
//Characters are stacked one, two, three when chosen at the beginning of
// the level. You get fifteen seconds, counted down and not announced to
// distribute your skillpoints.
// RADIAL SKILLPOINT MENUS!
// Character outfit should reflect how much power you have left, BOOMfist!

// music. ;_;

//TODO: that

func run() {
	rand.Seed(5435)
	//SLOW BOOT NOISE
	//	f, err := os.Open("media/boot.wav")
	f, err := os.Open("media/start-game.wav")

	done := make(chan struct{})
	s, format, _ := wav.Decode(f)

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	speaker.Play(beep.Seq(s, beep.Callback(func() {
		close(done)
	})))
	//speaker.Play(beep.Seq(bolt, beep.Callback(func() {
	//	close(eAttackS)
	//})))

	//MOVEDplay boot noise
	//<-done
	pic, err := LoadPicture("media/floor02.png")

	meltBookPic, err := LoadPicture("media/book-melt.png")
	phryPic, err := LoadPicture("media/phry.png")
	wallPic, err := LoadPicture("media/wall02.png")
	titlePic, err := LoadPicture("media/title1.png")
	startPic, err := LoadPicture("media/start.png")
	blobAnims := ImportSprite("media/bloop.png", 32)

	woonAnims := ImportSprite("media/woon-idll.png", 30)
	hoboAnims := ImportSprite("media/hobo-vibrate.png", 31)
	sparkleAnims := ImportSprite("media/cursor.png", 6)
	bloAnims := ImportSprite("media/blo.png", 32.0)
	//THIS SHOULD HAVE AN AWESOME BLOOOM EXPLOSION

	//THIS SHOULD HAVE A WICKED LIGHTNING BOLT EFFECT, AND SNOW FOR THE SNOW GODS
	randomAnims := ImportSprite("media/randomize-anim.png", 27)
	backdropPic, err := LoadPicture("media/backdrop.png")
	//feenish!
	if err != nil {
		fmt.Println("Error loading pictures")
		panic(err)
	}
	//Static sprites
	sprite := pixel.NewSprite(pic, pic.Bounds())
	wallSprite := pixel.NewSprite(wallPic, wallPic.Bounds())
	backdropSprite := pixel.NewSprite(backdropPic, backdropPic.Bounds())
	titleSprite := pixel.NewSprite(titlePic, titlePic.Bounds())
	startSprite := pixel.NewSprite(startPic, startPic.Bounds())
	//make the characters
	var PlayerOb Player

	//animated sprites
	frameWidth := 32.0
	var meltFrame []pixel.Rect
	meltFrame = SliceFrames(meltBookPic, frameWidth)
	var phryFrame []pixel.Rect
	phryFrame = SliceFrames(phryPic, frameWidth)

	//FOR THIS WE NEED THE POKERY
	//        anims := make(map[string][]pixel.Rect)

	cfg := pixelgl.WindowConfig{
		Title:  "Bits n Bobs",
		Bounds: pixel.R(0, 0, 1366, 768),
	}

	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	canvas := pixelgl.NewCanvas(pixel.R(-160/2, -120/2, 160/2, 120/2))

	var melanims []*pixel.Sprite
	for i := 0; i < len(meltFrame); i++ {
		spriter := pixel.NewSprite(meltBookPic, meltFrame[i])
		melanims = append(melanims, spriter)
	}
	var phryAnims []*pixel.Sprite
	for i := 0; i < len(phryFrame); i++ {
		spriter := pixel.NewSprite(phryPic, phryFrame[i])
		phryAnims = append(phryAnims, spriter)
	}

	PlayerOb.IdleAnims = woonAnims
	PlayerOb.SpriteIndex = 61
	PlayerOb.Idle = false
	//do a playing variable
	playing := 0
	position := 2
	i := 1
	phry := len(phryAnims)
	x := len(melanims)
	blob := len(blobAnims)
	blo := len(bloAnims)
	woon := len(woonAnims)

	hoboFrames := len(hoboAnims)

	started := false
	woonposx, woonposy := 658.0, 232.0
	choose := 0
	var Monsters []BlobMonsters
	for i := 0; i < 2; i++ {
		var bloob BlobMonsters
		bloob.Hp = 10
		bloob.Mp = 10
		bloob.Xpos = 400 + rand.Float64()*200
		bloob.Ypos = 230 + rand.Float64()*200
		copier.Copy(&bloob.Anims, &blobAnims)
		Monsters = append(Monsters, bloob)
	}
	blobposx, blobposy := 408.0, 300.0
	clearwin := false
	for !win.Closed() {
		win.SetCursorVisible(false)
		win.Clear(colornames.Black)

		if i > 10 {
			i = 10

		}
		for z := 0.0; z < 12.0; z++ {
			for y := 0.0; y < 12.0; y++ {
				wallSprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(165.0*z, 165.0*y)))

			}
		}

		if x > 0 {
			melanims[len(melanims)-x].Draw(win, pixel.IM.Scaled(pixel.ZV,
				math.Min(
					win.Bounds().W()/canvas.Bounds().W(),
					win.Bounds().H()/canvas.Bounds().H(),
				),
			).Moved(pixel.V(700.0, 275.0+float64(128+position))))
		} else {
			if phry > 0 {
				phryAnims[len(phryAnims)-phry].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(700.0, 275.0+float64(128+position))))
				phry--
				//Handle mouse sprite
				sparkleAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(win.MousePosition().X, win.MousePosition().Y)))
			} else {

				phry = 1
				phryAnims[len(phryAnims)-phry].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(700.0, 275.0+float64(128+position))))
				//Handle mouse sprite
				sparkleAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(win.MousePosition().X, win.MousePosition().Y)))
			}
		}

		//THIS IS A TERRIBLE IDEA, NEVER DO THIS
		//limit the framerate
		//time.Sleep(time.Millisecond * 150)

		var pos int
		pos = pos + (i * 16)

		sprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
			math.Min(
				win.Bounds().W()/canvas.Bounds().W(),
				win.Bounds().H()/canvas.Bounds().H(),
			),
		).Moved(pixel.V(700, 305)))
		//Handle mouse sprite
		sparkleAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
			math.Min(
				win.Bounds().W()/canvas.Bounds().W(),
				win.Bounds().H()/canvas.Bounds().H(),
			),
		).Moved(pixel.V(win.MousePosition().X, win.MousePosition().Y)))

		if win.JustPressed(pixelgl.MouseButton1) {
			clearwin = true
			cMusicF, _ := os.Open("media/c-select.wav")
			cMusic, _, _ := wav.Decode(cMusicF)
			speaker.Play(cMusic)
			for position == 2 {
				titleSprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Max(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(win.Bounds().Center()))
				//Handle mouse sprite
				sparkleAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(win.MousePosition().X, win.MousePosition().Y)))
				win.Update()

				if win.JustPressed(pixelgl.MouseButton1) {
					position = 0
				}
			}
		}
		playing++
		i++
		x--

		if clearwin {
			if err != nil {
				fmt.Println("Error decoding music!")
			}
			win.Clear(color.Black)
			for z := 0.0; z < 12.0; z++ {
				for y := 0.0; y < 12.0; y++ {
					wallSprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
						math.Min(
							win.Bounds().W()/canvas.Bounds().W(),
							win.Bounds().H()/canvas.Bounds().H(),
						),
					).Moved(pixel.V(165.0*z, 165.0*y)))
				}
			}
			backdropSprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
				math.Min(
					win.Bounds().W()/canvas.Bounds().W(),
					win.Bounds().H()/canvas.Bounds().H(),
				),
			).Moved(pixel.V(ChooseChar(choose))))
			//BEGIN ENEMY BLOCK
			randomAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
				math.Min(
					win.Bounds().W()/canvas.Bounds().W(),
					win.Bounds().H()/canvas.Bounds().H(),
				),
			).Moved(pixel.V(165.0, 165.0)))
			woonAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
				math.Min(
					win.Bounds().W()/canvas.Bounds().W(),
					win.Bounds().H()/canvas.Bounds().H(),
				),
			).Moved(pixel.V(woonposx, woonposy)))

			if hoboFrames >= 0 {
				//	fmt.Println(i)
				//	fmt.Println(len(Monsters[i].Anims))
				hoboAnims[len(hoboAnims)-hoboFrames].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(950, 225)))
			} else {
				hoboFrames = 1
				hoboAnims[len(hoboAnims)-hoboFrames].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(950, 225)))
			}

			blob--

			if blob > 0 {
				//	fmt.Println(i)
				//	fmt.Println(len(Monsters[i].Anims))
				blobAnims[len(blobAnims)-blob].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(blobposx, blobposy)))
			} else {
				blob = 1
				blobAnims[len(blobAnims)-blob].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(blobposx, blobposy)))
			}

			//END ENEMY BLOCK
			if woon > 0 {
				if win.JustPressed(pixelgl.KeyA) {
					choose--
					if choose < 0 {
						choose = 3
					}
					if choose == 3 {
						PlayerOb.IdleAnims = woonAnims
						PlayerOb.Xpos, PlayerOb.Ypos = woonposx, woonposy
						PlayerOb.Idle = true
						PlayerOb.IdleAnimName = "woon"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					} else {
						PlayerOb.Idle = false
					}
					if choose == 1 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = hoboAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 950, 225
						PlayerOb.IdleAnimName = "hobo"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose == 0 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = randomAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 165.0, 165.0
						PlayerOb.IdleAnimName = "rand"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					//TODO
					//make these choose the different characters and trigger their animations
				}
				if win.JustPressed(pixelgl.KeyD) {
					choose++
					if choose == 3 {
						PlayerOb.IdleAnims = woonAnims
						PlayerOb.Xpos, PlayerOb.Ypos = woonposx, woonposy
						PlayerOb.Idle = true
						PlayerOb.IdleAnimName = "woon"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					} else {
						PlayerOb.Idle = false
					}
					if choose == 1 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = hoboAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 950, 225
						PlayerOb.IdleAnimName = "hobo"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose == 0 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = randomAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 165.0, 165.0
						PlayerOb.IdleAnimName = "rand"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose >= 4 {
						choose = 0
					}
				}
				if win.JustPressed(pixelgl.KeyW) {
					choose++
					if choose == 3 {
						PlayerOb.IdleAnims = woonAnims
						PlayerOb.Xpos, PlayerOb.Ypos = woonposx, woonposy
						PlayerOb.Idle = true
						PlayerOb.IdleAnimName = "woon"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					} else {
						PlayerOb.Idle = false
					}
					if choose == 1 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = hoboAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 950, 225
						PlayerOb.IdleAnimName = "hobo"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose == 0 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = randomAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 165.0, 165.0
						PlayerOb.IdleAnimName = "rand"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose >= 4 {
						choose = 1
					}
				}
				if win.JustPressed(pixelgl.KeyS) {
					choose--
					if choose == 3 {
						PlayerOb.IdleAnims = woonAnims
						PlayerOb.Xpos, PlayerOb.Ypos = woonposx, woonposy
						PlayerOb.Idle = true
						PlayerOb.IdleAnimName = "woon"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					} else {
						PlayerOb.Idle = false
					}
					if choose == 1 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = hoboAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 950, 225
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
						PlayerOb.IdleAnimName = "hobo"
					}
					if choose == 0 {
						PlayerOb.Idle = true
						PlayerOb.IdleAnims = randomAnims
						PlayerOb.Xpos, PlayerOb.Ypos = 165.0, 165.0
						PlayerOb.IdleAnimName = "rand"
						PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
					}
					if choose < 0 {
						choose = 3
					}
				}
				//UNDER CONSTRUCTION
				//TODO:  THIS
				if win.JustPressed(pixelgl.KeySpace) {
					started = true
				}
				if win.JustPressed(pixelgl.MouseButton1) {
					//checkX, checkY := win.MousePosition().XY()

				}

				if started {
					startSprite.Draw(win, pixel.IM.Scaled(pixel.ZV,
						math.Min(
							win.Bounds().W()/canvas.Bounds().W(),
							win.Bounds().H()/canvas.Bounds().H(),
						),
					).Moved(win.Bounds().Center()))
					win.Update()
					time.Sleep(3 * time.Second)
					//started = false
					woon = -1
				}

				//	if win.JustPressed(pixelgl.KeySpace) {
				//		attacking = true
				//		attack = "poke"
				//		pokeFile, err = os.Open("media/awesome.wav")
				//		poke, _, _ = wav.Decode(pokeFile)
				//		sAttackS = make(chan struct{})
				//	}
				if win.JustPressed(pixelgl.KeyE) {

					//					speaker.Play(bolt)
				}

				//THIS WORKS NO MATTER WHAT YOU SET THE PLAYEROB ANIMS TO
				//KEEP FOR FURTHER USE
				switch PlayerOb.IdleAnimName {
				case "rand":
					if PlayerOb.Idle {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(PlayerOb.Xpos, PlayerOb.Ypos)))
						PlayerOb.SpriteIndex--

						if PlayerOb.SpriteIndex <= 0 {
							PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
						}
					} else {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(PlayerOb.Xpos, PlayerOb.Ypos)))
					}
				case "woon":
					if PlayerOb.Idle {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(woonposx, woonposy)))
						PlayerOb.SpriteIndex--

						if PlayerOb.SpriteIndex <= 0 {
							PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
						}
					} else {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(woonposx, woonposy)))
					}
				case "hobo":

					if PlayerOb.Idle {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(PlayerOb.Xpos, PlayerOb.Ypos)))
						PlayerOb.SpriteIndex--

						if PlayerOb.SpriteIndex <= 0 {
							PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
						}
					} else {
						PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
							math.Min(
								win.Bounds().W()/canvas.Bounds().W(),
								win.Bounds().H()/canvas.Bounds().H(),
							),
						).Moved(pixel.V(PlayerOb.Xpos, PlayerOb.Ypos)))
					}

					//KEEP FOR FURTHER USE
				}

			}
			//this 'kinda' works
			if started {
				win.Clear(colornames.Blanchedalmond)
				if PlayerOb.SpriteIndex <= 0 {
					PlayerOb.SpriteIndex = len(PlayerOb.IdleAnims)
				}
				PlayerOb.IdleAnims[len(PlayerOb.IdleAnims)-PlayerOb.SpriteIndex].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(pixel.V(PlayerOb.Xpos, PlayerOb.Ypos)))
				PlayerOb.SpriteIndex--
				if blo <= 0 {
					blo = len(bloAnims)
				}
				bloAnims[len(bloAnims)-blo].Draw(win, pixel.IM.Scaled(pixel.ZV,
					math.Min(
						win.Bounds().W()/canvas.Bounds().W(),
						win.Bounds().H()/canvas.Bounds().H(),
					),
				).Moved(win.Bounds().Center()))
				blo--
				//movements
				if win.Pressed(pixelgl.KeyW) {
					PlayerOb.Ypos += 6
				}
				if win.Pressed(pixelgl.KeyS) {
					PlayerOb.Ypos -= 6
				}
				if win.Pressed(pixelgl.KeyA) {
					PlayerOb.Xpos -= 6
				}
				if win.Pressed(pixelgl.KeyD) {
					PlayerOb.Xpos += 6
				}
			}
			//Handle mouse sprite
			sparkleAnims[0].Draw(win, pixel.IM.Scaled(pixel.ZV,
				math.Min(
					win.Bounds().W()/canvas.Bounds().W(),
					win.Bounds().H()/canvas.Bounds().H(),
				),
			).Moved(pixel.V(win.MousePosition().X, win.MousePosition().Y)))
			win.Update()
		} else {
			win.Update()
		}
		//PLAY BOOT NOISE
		for i := 0; i < 1; i++ {
			<-done
		}
	}

	if err != nil {
		fmt.Println("Something went wrong!")
	}
}

func main() {
	pixelgl.Run(run)

}
